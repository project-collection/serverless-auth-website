# serverless-auth-website

Build dynamic website with authentication/backend with serverless framework.

## Intro

![Architecture](docs/serverless-auth-website.PNG)


tutorial: https://docs.google.com/document/d/1hVk0fjvFQzPG_yXf1CRtWmsiupWV-EskH75NIlzf9KM/edit#

## Stack


```bash
aws cloudformation create-stack --stack-name testwebsite --template-body file://dynamodb_s3_cloudfront.yml --region eu-central-1
```

## ToDo:

- security enhancement: make content in s3 bucket inaccessible directly
- improve frontend UI
- package cloudfront into cfn stack
- write cloudfront authentication lambda logic
- create mocked users in dynamodb
- load static files into S3Bucket
