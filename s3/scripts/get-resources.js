
// output what stored in local Storage
console.log(localStorage.getItem("idToken"));
console.log(localStorage.getItem("accessToken"));
console.log(localStorage.getItem("name"));


var name = localStorage.getItem("name");
const url = 'https://78vr4jesnh.execute-api.eu-central-1.amazonaws.com/prod?name='+name;


// make http requst to get folders list
fetch(url, {
  method: "GET",
  headers: {
    "Authorization" : localStorage.getItem("idToken"),
    "Content-Type": "application/json",
  },
}).then(function(response) {
    console.log("Success");
    return response.json();
})
.then(function(items) {
    // get list object on resources.html
    var ul = document.getElementById('myItemList');

    items.forEach(function (item) {
      var li = document.createElement('li');
      ul.appendChild(li);
      li.innerHTML += item;
    });
})
.catch(function() {
    console.log("Failed")
});
