document.getElementById('signin').onclick = function (){
  // fetch user input from form
  var email = document.getElementById('email').value ;
  var password = document.getElementById('password').value ;
  // construct authenticationData object
  var authenticationData = {
      Username : email,
      Password : password,
  };
  // construct authenticationDetails object
  var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);
  // construct cognito user pool data
  var poolData = { UserPoolId : 'eu-central-1_4x2Jd7cKX',
      ClientId : '4jb7qrim8e7s1iudbtu29l4c7t'
  };
  var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
  // construct cognito user object
  var userData = {
      Username : email,
      Pool : userPool
  };
  localStorage.setItem("name", email);

  // authenticating user process
  var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);
  cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
          var accessToken = result.getAccessToken().getJwtToken();
          console.log(accessToken)
          /* Use the idToken for Logins Map when Federating User Pools with identity pools or when passing through an Authorization Header to an API Gateway Authorizer*/
          var idToken = result.idToken.jwtToken;
          // store token information locally
          localStorage.setItem("accessToken", accessToken);
          localStorage.setItem("idToken", idToken)
          // by success show a list of resources
          window.location.href="resources.html";
      },
      onFailure: function(err) {
          alert(err);
      }
  });
}

// forget password process
// verification code will be sent through registered email
// two prompt to confirm password reset
document.getElementById("forgetpassword").onclick = function(){

  var email = document.getElementById('email').value;
  // construct cognito user pool data
  var poolData = { UserPoolId : 'eu-central-1_4x2Jd7cKX',
                   ClientId : '4jb7qrim8e7s1iudbtu29l4c7t'};
  var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
  var userData = {
      Username : email,
      Pool : userPool
  };
  var cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

  cognitoUser.forgotPassword({
    onSuccess: function (result) {
        console.log('call result: ' + result);
    },
    onFailure: function(err) {
        alert(err);
    },
  inputVerificationCode() {
        var verificationCode = prompt('Please input verification code ' ,'');
        var newPassword = prompt('Enter new password ' ,'');
        cognitoUser.confirmPassword(verificationCode, newPassword, this);
    }
});
}
