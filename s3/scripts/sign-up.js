var user_pool_id = 'eu-central-1_4x2Jd7cKX';
var client_id = '4jb7qrim8e7s1iudbtu29l4c7t';

// call function if signup button on signup.html page is clicked
document.getElementById('signup').onclick = function () {
  // cognito user pool data
  var poolData = {
    UserPoolId : user_pool_id,
    ClientId : client_id
};

// You can find the javascript SDK cognito authentication example here https://docs.aws.amazon.com/cognito/latest/developerguide/using-amazon-cognito-user-identity-pools-javascript-examples.html
// create an cognito user pool object
var userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);

// get user input from <input> box
var attributeList = [];
var email = document.getElementById('email').value ;
var password = document.getElementById('password').value ;
// build attributeList which is required parameter for signUp function
var dataEmail = {
    Name : 'email',
    Value : email
};

var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
attributeList.push(attributeEmail);

// sign up the user
// if fail an alert will be shown
// if success user name will be outputed in console
var cognitoUser;
userPool.signUp(email, password, attributeList, null, function(err, result){
      // pop up alert if signup error
      if (err) {
          alert(err);
          return;
      }

      // log the signed up username
      cognitoUser = result.user;
      console.log('user name is ' + cognitoUser.getUsername());
  });
}
